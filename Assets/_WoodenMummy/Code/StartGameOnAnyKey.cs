﻿using System.Collections;
using System.Collections.Generic;
using PixelFr0ggie;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
using XboxCtrlrInput;
using XInputDotNetPure;
#endif

[RequireComponent(typeof(LoadScene))]
public class StartGameOnAnyKey : MonoBehaviour
{
	public Text instructionText;

	// Update is called once per frame
	void Update()
	{
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		if (GamePad.GetState(0).IsConnected)
		{
			if (instructionText)
				instructionText.text = "Press X to Begin";

			if (GamePad.GetState(0).Buttons.X == ButtonState.Pressed)
				StartGame();
		}
		else if (!GamePad.GetState(0).IsConnected)
		{
			if (instructionText)
				instructionText.text = "Press Return to Begin";

			if (Input.GetKeyDown(KeyCode.Return))
				StartGame();
		}
#else
		if (instructionText)
			instructionText.text = "Press Return to Begin";

		if (Input.GetKeyDown(KeyCode.Return))
			StartGame();
#endif
	}

	void StartGame()
	{
		//SceneManager.LoadScene("Game");
		GetComponent<LoadScene>().Activate();
	}
}