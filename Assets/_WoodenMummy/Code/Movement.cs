﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
using XInputDotNetPure;

[RequireComponent(typeof(Controller))]
public class Movement : MonoBehaviour
{
	[SerializeField] private bool bUseController = false;
	public float movementSpeed = 4.0f;
	public float rotationSpeed = 4.0f;
	public float responsiveness = 14.0f;

	public float movementSpeedRandomness = 0.0f;
	public float responsivenessRandomness = 0.0f;

	public float NPCDistanceResponse;

	public static List<Movement> otherNPCS = new List<Movement>();

	public Controller controller;
	public new Rigidbody rigidbody;

	public Animator characterAnimator;

	public bool BUseController
	{
		get { return bUseController; }
		set { bUseController = value; }
	}

	private void Start()
	{
		movementSpeed = Random.Range(movementSpeed - movementSpeedRandomness / 2, movementSpeed + movementSpeedRandomness / 2);
		responsiveness = Random.Range(responsiveness - responsivenessRandomness / 2, responsiveness + responsivenessRandomness / 2);
	}

	private void OnEnable()
	{
		otherNPCS.Add(this);
	}

	private void OnDisable()
	{
		otherNPCS.Remove(this);
	}

	void Update()
	{
		if (controller == null)
			return;

		if (BUseController)
		{
			rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,
				controller.desiredDirection * movementSpeed, Time.deltaTime *
				responsiveness);

			transform.LookAt(controller.desiredLookatLocation);

		}

		if (characterAnimator)
		{
			characterAnimator.SetFloat("Speed", rigidbody.velocity.magnitude);
		}
		//AI Movement
		//else
		//{
		//    Vector3 playerPos = PlayerManager.Instance.firstPlayer.transform.position;
		//    Vector3 runFromPlayer = transform.position - playerPos;

		//    if (Mathf.Abs(Vector3.Distance(transform.position, playerPos)) < NPCDistanceResponse)
		//    {
		//        rigidbody.velocity =
		//            Vector3.Lerp(rigidbody.velocity,
		//                runFromPlayer.normalized *
		//                movementSpeed, Time.deltaTime *
		//                responsiveness);
		//    }
		//}
	}
}