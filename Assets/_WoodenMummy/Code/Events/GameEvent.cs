using System.Collections.Generic;
using UnityEngine;

namespace KoriKori.Events
{
    [CreateAssetMenu]
    public class GameEvent : ScriptableObject
    {
        public bool debugRaise = false;

        private List<GameEventListener> eventListeners =
            new List<GameEventListener>();

        public void Raise()
        {
            if (debugRaise)
                Debug.Log("Event: " + this.name + "was raised!");

            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised();
        }

        public void RegisterListener(GameEventListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(GameEventListener listener)
        {
            eventListeners.Remove(listener);
        }

    }
}
