﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
	public Animator characterAnimator;
	public void DoAttack()
	{
		if (characterAnimator)
		{
			characterAnimator.SetTrigger("Swipe");
		}
	}
}