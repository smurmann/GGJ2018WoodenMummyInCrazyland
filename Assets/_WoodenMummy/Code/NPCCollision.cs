﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCollision : MonoBehaviour
{
	public MeshRenderer mesh;
	public bool bIsTriggered = false;

	void OnTriggerEnter(Collider other)
	{
		Debug.Log(other.name);

		Infect();

	}

	public void Infect()
	{
		if (bIsTriggered)
			return;

		bIsTriggered = true;

		GetComponent<Movement>().BUseController = true;

		mesh.material.color = Color.red;

	}
}