﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PixelFr0ggie
{
    public class AudioManager : Singleton<AudioManager>
    {
        public MasterAudio masterAudio;
        public PlaylistController playlistController;

        public MasterAudio MasterAudio { get { return masterAudio; } }
        public PlaylistController PlaylistController { get { return playlistController; } }

    }
}
