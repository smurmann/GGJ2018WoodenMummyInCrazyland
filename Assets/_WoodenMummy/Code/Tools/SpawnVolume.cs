﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpawnVolume : MonoBehaviour
{
	[SerializeField]
	public List<ObjectSpawnOptions> objectToSpawn;

	public BoxCollider bounds;

	public bool spawnOnStart = false;

	void Start()
	{
		if (spawnOnStart)
			SpawnEverything();
	}

	public void SpawnEverything()
	{
		foreach (ObjectSpawnOptions spawnOption in objectToSpawn)
		{

			string objectName = "Container: " +
				spawnOption.objectToSpawn.name;

			GameObject spawnParent = GameObject.Find(objectName);

			if (spawnParent)
			{
				if (!Application.isPlaying)
				{
					DestroyImmediate(spawnParent);
				}
				else
				{
					Destroy(spawnParent);
				}
			}

			spawnParent = new GameObject(objectName);

			for (int i = 0; i < spawnOption.numberOfObjects; i++)
			{
				Vector3 randomPos = bounds.transform.position;

				Vector3 size = bounds.size;

				randomPos += new Vector3(
					(Random.value - 0.5f)* size.x,
					0,
					(Random.value - 0.5f)* size.z
				);

				Vector3 randomEulerRotation = new Vector3();

				if (spawnOption.bUsed45DegreeAngles)
					randomEulerRotation = new Vector3(0, Random.Range(1, 3)* 90, 0);
				else
				{
					randomEulerRotation =
						new Vector3(0, Random.value * 360, 0);

				}

				Instantiate(
					spawnOption.objectToSpawn, randomPos,
					Quaternion.Euler(randomEulerRotation),
					spawnParent.transform);
			}
		}
	}
}

[System.Serializable]
public class ObjectSpawnOptions
{
	[SerializeField]
	public GameObject objectToSpawn;

	[SerializeField]
	public int numberOfObjects;

	[SerializeField]
	public bool bUsed45DegreeAngles = true;
}