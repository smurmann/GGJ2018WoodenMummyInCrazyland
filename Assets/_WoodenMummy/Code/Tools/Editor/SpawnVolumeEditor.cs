﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SpawnVolume))]
public class SpawnVolumeEditor : Editor
{
	public override void OnInspectorGUI()
	{
		SpawnVolume myTarget = (SpawnVolume)target;

		if (GUILayout.Button("Generate Spawns"))
		{
			myTarget.SpawnEverything();
		}

		base.OnInspectorGUI();
	}
}