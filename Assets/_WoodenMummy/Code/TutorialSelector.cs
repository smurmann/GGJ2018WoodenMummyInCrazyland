﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
using XboxCtrlrInput;
using XInputDotNetPure;
#endif

public class TutorialSelector : MonoBehaviour
{

	public GameObject xboxTutorial;
	public GameObject pcTutorial;

	void Update()
	{
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		if (GamePad.GetState(0).IsConnected)
		{
			if (xboxTutorial)
				xboxTutorial.SetActive(true);
			if (pcTutorial)
				pcTutorial.SetActive(false);

		}
		else if (!GamePad.GetState(0).IsConnected)
		{
			if (pcTutorial)
				pcTutorial.SetActive(true);
			if (xboxTutorial)
				xboxTutorial.SetActive(false);
		}

#else
		if (pcTutorial)
			pcTutorial.SetActive(true);
		if (xboxTutorial)
			xboxTutorial.SetActive(false);
#endif
	}
}