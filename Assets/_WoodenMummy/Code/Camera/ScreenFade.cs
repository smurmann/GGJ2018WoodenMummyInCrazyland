﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenFade : MonoBehaviour
{

	public Texture2D fadeOutTexture;
	public float fadeSpeed = 0.8f;

	private int _drawDepth = -1000;
	private float _alpha = 1.0f;
	private int _fadeDir = -1; // the direction to fade: in = -1 or out -1

	void OnGUI()
	{
		_alpha += _fadeDir * fadeSpeed * Time.deltaTime;
		_alpha = Mathf.Clamp01(_alpha);

		GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, _alpha);
		GUI.depth = _drawDepth;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
	}

	public void MaxAlpha()
	{

		_fadeDir = 0;
		_alpha = 1;
	}

	public void FadeOut()
	{
		BeginFade(-1);
	}

	public float BeginFade(int direction)
	{
		_fadeDir = direction;
		return (fadeSpeed);
	}

	void OnEnable()
	{
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		_alpha = 1;
		BeginFade(-1);
	}

	// called when the game is terminated
	void OnDisable()
	{
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

}