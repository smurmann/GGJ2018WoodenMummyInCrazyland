using System.Collections;
using System.Collections.Generic;
using Thinksquirrel.CShake;
using UnityEngine;

namespace KoriKori
{

    [RequireComponent(typeof(Camera))]
    public class MainCamera : MonoBehaviour
    {
        public Transform player = null;

        [RangeAttribute(0f, 2f)]
        public float targetBlendWeight = 0.5f;
        public float zoomPerNPC = 0.5f;
        public float maxZoomAmount = 5.0f;
        public float cameraFollowSpeed = 1.0f;

        private Vector3 avgPos = new Vector3();
        private float yPos;
        private float xDif;
        private float zDif;

        private Vector3 originalZoom;

        public void Awake()
        {
            Application.targetFrameRate = 300;
        }

        public void Start()
        {
            Initialize(player);
        }

        public void Initialize(Transform newPlayer)
        {
            //GetComponent<Camera>().fieldOfView = 60;
            var CurrentPos = transform.position;
            CurrentPos.x = newPlayer.position.x;
            //CurrentPos.z = newPlayer.position.z - 15f;
            transform.position = CurrentPos;

            player = newPlayer;
            yPos = transform.position.y;
            xDif = player.position.x - this.transform.position.x;
            zDif = player.position.z - this.transform.position.z;
            originalZoom = player.position - transform.position;

        }

        List<Vector3> GetHoardAverage()
        {
            List<Vector3> positions = new List<Vector3>();
            foreach (Movement mov in Movement.otherNPCS)
            {
                if (mov.BUseController)
                    positions.Add(mov.transform.position);
            }

            avgPos = GetMeanVector(positions);

            return positions;
        }

        private Vector3 GetMeanVector(List<Vector3> positions)
        {
            if (positions.Count == 0)
                return Vector3.zero;

            float x = 0f;
            float y = 0f;
            float z = 0f;

            foreach (Vector3 pos in positions)
            {
                x += pos.x;
                y += pos.y;
                z += pos.z;
            }
            return new Vector3(x / positions.Count, y / positions.Count, z / positions.Count);
        }

        void Update()
        {
            if (player == null)
                return;

            List<Vector3> hoardPositions = GetHoardAverage();

            Vector3 targetPosition = new Vector3 (Mathf.Lerp(player.position.x, avgPos.x, targetBlendWeight) - xDif,
                                                    yPos, 
                                                    Mathf.Lerp(player.position.z, avgPos.z, targetBlendWeight) - zDif);


            //this.transform.position = new Vector3(Mathf.Lerp(this.transform.position.x, targetPosition.x, Time.deltaTime * cameraFollowSpeed),
            //                                        yPos, 
            //                                        Mathf.Lerp(this.transform.position.z, targetPosition.z, Time.deltaTime * cameraFollowSpeed));

            float zoomAmount = zoomPerNPC * hoardPositions.Count;
            zoomAmount = zoomAmount > maxZoomAmount ? maxZoomAmount : zoomAmount;
            Vector3 targetZoom = -originalZoom.normalized * zoomAmount;

            Vector3 finalPosition = targetPosition + targetZoom;

            this.transform.position = new Vector3(Mathf.Lerp(transform.position.x, finalPosition.x, Time.deltaTime * cameraFollowSpeed),
                                                    Mathf.Lerp(transform.position.y, finalPosition.y, Time.deltaTime * cameraFollowSpeed),
                                                    Mathf.Lerp(transform.position.z, finalPosition.z, Time.deltaTime * cameraFollowSpeed));

        }

    }
}