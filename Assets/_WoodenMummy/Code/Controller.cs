﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
using XboxCtrlrInput;
using XInputDotNetPure;
#endif

public class Controller : MonoBehaviour
{
	public float desiredRotationDirection = 0.0f;
	public Vector3 desiredLookatLocation;
	public Vector3 desiredDirection;
	public float dashDelay = 0.5f;
	public SphereCollider colliderToActivate;
	public string attackSoundName = "Whiff";
	public string dashSoundName = "Powerup";
	public bool playMummyLaughSound = false;
	public string mummyLaughSoundName = "Mummy Laugh";

	private float dashTimer = 0.0f;
	private bool canAttack = true;
	private bool canDash = true;
	private bool isPlayingMummyLaugh = false;

	protected virtual void Update()
	{
		if (GameManager.Instance.gameFinished)
			return;

		MovementControl();
		AttackControl();
	}

	protected virtual void MovementControl()
	{
		Vector3 newDirection = new Vector3();

		float X = 0.0f;
		float Y = 0.0f;

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		if (GamePad.GetState(0).IsConnected)
		{
			var thumbSticks = GamePad.GetState(0).ThumbSticks;
			X = thumbSticks.Left.X;
			Y = thumbSticks.Left.Y;
		}
		else
		{
			X = Input.GetAxis("Horizontal");
			Y = Input.GetAxis("Vertical");
		}
#else
		X = Input.GetAxis("Horizontal");
		Y = Input.GetAxis("Vertical");
#endif

		newDirection.x = X;
		newDirection.z = Y;

		desiredLookatLocation = transform.position + new Vector3(X, 0, Y);
		desiredDirection = newDirection.normalized;

		if (newDirection.sqrMagnitude >= 0.005f)
		{

			if (!isPlayingMummyLaugh)
			{
				if (playMummyLaughSound)
				{
					isPlayingMummyLaugh = true;
					MasterAudio.PlaySound(mummyLaughSoundName);
				}
			}
		}
		else
		{
			if (isPlayingMummyLaugh)
			{
				isPlayingMummyLaugh = false;
				MasterAudio.StopAllOfSound(mummyLaughSoundName);
			}

		}
	}

	protected virtual void AttackControl()
	{
		if (GetComponent<Movement>().BUseController == false)
			return;

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		if ((GamePad.GetState(0).Buttons.RightShoulder == ButtonState.Released || Input.GetButtonUp("Fire1"))&& !canAttack)
		{
			canAttack = true;
		}
#else
		if ((Input.GetButtonUp("Fire1"))&& !canAttack)
		{
			canAttack = true;
		}
#endif

#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		if ((GamePad.GetState(0).Buttons.RightShoulder == ButtonState.Pressed || Input.GetButtonDown("Fire1"))&& canAttack)
#else
			if (Input.GetButtonDown("Fire1")&& canAttack)
#endif
		{
			canAttack = false;

			if (GetComponent<Attack>())
			{
				GetComponent<Attack>().DoAttack();
				MasterAudio.PlaySound(attackSoundName);
			}
		}
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		if ((GamePad.GetState(0).Buttons.A == ButtonState.Pressed || Input.GetButtonDown("Fire2"))&& dashTimer >= dashDelay)
#else
			if (Input.GetButtonDown("Fire2")&& dashTimer >= dashDelay)
#endif
		{
			//canDash = false;
			dashTimer = 0.0f;

			if (GetComponent<Rigidbody>())
			{
				var rb = GetComponent<Rigidbody>();

				rb.velocity = rb.velocity.normalized * 50f;
				MasterAudio.PlaySound(dashSoundName);
			}
		}

		dashTimer += Time.deltaTime;
	}

}