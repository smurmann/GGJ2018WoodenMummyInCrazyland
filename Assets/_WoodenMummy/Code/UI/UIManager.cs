﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	private int initialNPCCount;

	public int countTillMaxBar = 50;

	public Image topMeter;
	public int npcsTurned = 0;

	public Animator worldStateAnimator;
	public Text worldStateText;

	public Text humanMeterText;

	public string worldState = "";
	public int humanCount = 0;

	private static UIManager instance;

	public static UIManager Instance
	{
		get
		{
			return instance;
		}

		set
		{
			instance = value;
		}
	}

	void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		initialNPCCount = 0;

		foreach (Movement npcs in Movement.otherNPCS)
		{
			if (npcs.BUseController == false)
			{
				initialNPCCount++;
			}
		}

		MonsterCountUpdated();
	}

	public void MonsterCountUpdated()
	{
		int npcCount = 0;

		foreach (Movement npcs in Movement.otherNPCS)
		{
			if (npcs.BUseController == false)
			{
				npcCount++;
			}
		}

		if (humanMeterText)
			humanMeterText.text = "HUMANS LEFT: " + npcCount;

		humanCount = npcCount;

		npcsTurned = Mathf.Abs(npcCount - initialNPCCount);

		float fillAmount = topMeter.fillAmount = (float) npcsTurned / (float) countTillMaxBar;

		UpdateWorldState(fillAmount);
	}

	private void UpdateWorldState(float percentage)
	{
		if (percentage >= 0 && percentage < 0.2f)
		{
			if (worldStateAnimator)
				worldStateAnimator.Play("Intensity0");

			worldState = "Serene Day";
		}
		else if (percentage >= 0.2f && percentage < 0.4f)
		{
			if (worldStateAnimator)
				worldStateAnimator.Play("Intensity1");

			worldState = "Mummification";
		}
		else if (percentage >= 0.4f && percentage < 0.6f)
		{
			if (worldStateAnimator)
				worldStateAnimator.Play("Intensity2");

			worldState = "Craaaazzzzyland";
		}
		else if (percentage >= 0.6f && percentage < 0.8f)
		{
			if (worldStateAnimator)
				worldStateAnimator.Play("Intensity3");

			worldState = "Doomsday";
		}
		else if (percentage >= 0.8f)
		{
			if (worldStateAnimator)
				worldStateAnimator.Play("Intensity4");

			worldState = "Pandemonium";
		}

		if (worldStateText)
			worldStateText.text = worldState;
	}
}