﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
using XboxCtrlrInput;
using XInputDotNetPure;
#endif

public class EndScreen : MonoBehaviour
{
	public Text numberOfHumans;
	public Text worldState;
	public Text instructions;
	public Text backToMenuInstructions;

	void OnEnable()
	{
		numberOfHumans.text = UIManager.Instance.humanCount.ToString();
		worldState.text = UIManager.Instance.worldState;
	}

	// Update is called once per frame
	void Update()
	{
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		if (GamePad.GetState(0).IsConnected)
		{
			if (instructions)
				instructions.text = "Press Enter to Play Again!";

			if (backToMenuInstructions)
				backToMenuInstructions.text = "Press Back to return to menu.";

			if (GamePad.GetState(0).Buttons.X == ButtonState.Pressed)
				GameManager.Instance.RestartGame();

			if (GamePad.GetState(0).Buttons.Back == ButtonState.Pressed)
				GameManager.Instance.RestartGame();
		}
		else if (!GamePad.GetState(0).IsConnected)
		{
			if (instructions)
				instructions.text = "Press Enter to Play Again!";

			if (backToMenuInstructions)
				backToMenuInstructions.text = "Press Escape go back to menu.";

			if (Input.GetKeyDown(KeyCode.Return))
				GameManager.Instance.RestartGame();

			if (Input.GetKeyDown(KeyCode.Escape))
				GameManager.Instance.ReturnToMenu();
		}
#else
		if (instructions)
			instructions.text = "Press Enter to Play Again!";

		if (backToMenuInstructions)
			backToMenuInstructions.text = "Press Escape go back to menu.";

		if (Input.GetKeyDown(KeyCode.Return))
			GameManager.Instance.RestartGame();

		if (Input.GetKeyDown(KeyCode.Escape))
			GameManager.Instance.ReturnToMenu();
#endif
	}
}