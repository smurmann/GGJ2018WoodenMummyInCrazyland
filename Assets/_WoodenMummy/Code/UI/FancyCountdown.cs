﻿using System.Collections;
using System.Collections.Generic;
using KoriKori.Events;
using UnityEngine;
using UnityEngine.UI;

public class FancyCountdown : MonoBehaviour
{
	public float countdownFrom = 30f;
	private Text countDownText;
	public string finalCountWord = "FINISH";
	public GameEvent OnGameFinished;

	public bool bStartCounting = true;
	public bool bShouldStartGame = false;

	private void Start()
	{
		countDownText = GetComponent<Text>();

		if (countDownText)
		{
			countDownText.text = countdownFrom.ToString("f0");
			PlayFancyAnimation();
		}
	}

	private void Update()
	{
		if (!bStartCounting)
			return;

		if (!countDownText)
			return;

		countdownFrom -= Time.deltaTime;

		if (countdownFrom <= -0.5f)
		{
			countDownText.text = "";
			bStartCounting = false;

			if (OnGameFinished)
				OnGameFinished.Raise();

			if (bShouldStartGame)
				GameManager.Instance.StartGame();
			return;
		}

		if (countdownFrom.ToString("f0")!= countDownText.text)
		{
			if (countdownFrom.ToString("f0")== "0")
			{
				FinishCountdown();
				return;
			}

			countDownText.text = countdownFrom.ToString("f0");
			PlayFancyAnimation();
		}
	}

	void PlayFancyAnimation()
	{
		if (GetComponent<Animator>())
		{
			GetComponent<Animator>().Play("BounceUI");
		}
	}

	public void FinishCountdown()
	{
		countDownText.text = finalCountWord;
	}

}