﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

public class ToggleCollision : MonoBehaviour
{

	public SphereCollider damageCollider;
    public bool playWalkSound = false;
    public string walkSoundName = "Walk";

    public void SetCollisionEnabled(int on)
	{
		if (damageCollider)
		{
			switch (on)
			{
				case 0:
					damageCollider.enabled = false;
					break;
				case 1:
					damageCollider.enabled = true;
					break;
			}

		}
	}

    public void PlayWalkSound()
    {
        if (playWalkSound)
            MasterAudio.PlaySound(walkSoundName);
    }
}