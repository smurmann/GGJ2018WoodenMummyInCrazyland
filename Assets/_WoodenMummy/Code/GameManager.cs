﻿using System.Collections;
using System.Collections.Generic;
using PixelFr0ggie;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{

	private static GameManager instance;

	public FancyCountdown countdownTimer;

	public string bgmName = "BGM";

	public LoadScene replayScene;

	public LoadScene returnScene;

	public static GameManager Instance
	{
		get
		{
			return instance;
		}

		set
		{
			instance = value;
		}
	}

	void Awake()
	{
		Instance = this;
	}

	public bool gameFinished = false;
	public void SetGameFinished()
	{
		gameFinished = true;
	}

	public void StartGame()
	{
		AudioManager.Instance.PlaylistController.StartPlaylist(bgmName);
		PlayerManager.Instance.firstPlayer.GetComponent<Controller>().enabled = true;
		PlayerManager.Instance.firstPlayer.GetComponent<Movement>().enabled = true;
		countdownTimer.bStartCounting = true;
	}

	public void RestartGame()
	{
		//UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
		replayScene.Activate();
	}

	public void ReturnToMenu()
	{
		//UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
		returnScene.Activate();
	}
}