﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rumbler : MonoBehaviour
{

	public void LightRumble()
	{
		PlayerManager.Instance.VibrateController(0.0f, 0.1f, 0.2f);
	}

	public void HeavyRumble()
	{
		PlayerManager.Instance.VibrateController(0.1f, 0.0f, 0.2f);
	}
}