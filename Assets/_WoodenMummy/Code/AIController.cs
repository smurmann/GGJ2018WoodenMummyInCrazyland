﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using KoriKori.Events;
using Thinksquirrel.CShake;
using UnityEngine;

namespace PixelFr0ggie
{
    public class AIController : Controller
    {
        public GameEvent OnChangeTriggered;
        public static List<AIController> aiControllers = new List<AIController>();
        public SkinnedMeshRenderer mesh = null;
        public SteeringBasics steering = null;
        public CollisionAvoidance collisionAvoidance = null;
        public Flee flee = null;
        public Cohesion cohesion = null;
        public Separation separation = null;
        public VelocityMatch velocityMatch = null;
        public Wander2 wander = null;
        public NearSensor collisionSensor;
        public NearSensor flockingSensor;
        public bool playCamShakeOnHit = false;
        public CameraShakeData camShakeOnHit = new CameraShakeData();
        public string hitSoundName = "Slap";
        public string infectSoundName = "Human Converted";
        public Color infectRimColor = Color.red;
        public Color infectShadowColor = Color.red;
        public Color infectHighlightColor = Color.red;
        public Color infectColor = Color.red;
        private Color originalColor = Color.blue;

        public float copyPlayerRadius = 5.0f;
        public float collisionAvoidanceWeight = 2f;
        public float cohesionWeight = 1.5f;
        public float separationWeight = 2f;
        public float velocityMatchWeight = 1f;

        public LayerMask hitMask = -1;

        private Transform playerTransform = null;
        private bool bIsTriggered = false;

        public GameObject effectToSpawn;
        public GameEvent onInfected;

        public bool BIsTriggered
        {
            get { return bIsTriggered; }
            private set
            {
                if (bIsTriggered != value)
                {
                    bIsTriggered = value;
                    if (OnChangeTriggered)
                        OnChangeTriggered.Raise();
                }

            }
        }

        private void OnEnable()
        {
            aiControllers.Add(this);
        }

        private void OnDisable()
        {
            aiControllers.Remove(this);
        }

        // Use this for initialization
        protected void Start()
        {
            if (!steering)steering = GetComponent<SteeringBasics>();
            if (!flee)flee = GetComponent<Flee>();
            if (!cohesion)cohesion = GetComponent<Cohesion>();
            if (!separation)separation = GetComponent<Separation>();
            if (!velocityMatch)velocityMatch = GetComponent<VelocityMatch>();
            if (!wander)wander = GetComponent<Wander2>();

            playerTransform = PlayerManager.Instance.firstPlayer.transform;
            originalColor = mesh.material.color;

        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other != colliderToActivate &&
                hitMask == (hitMask | (1 << other.gameObject.layer)))
            {
                MasterAudio.PlaySound(hitSoundName);
                Infect();
            }
        }

        protected override void MovementControl()
        {
            if (!bIsTriggered)
            {
                Vector3 accel = flee.getSteering(playerTransform.position);
                if (accel.magnitude < 0.005f)accel = wander.getSteering();
                accel += collisionAvoidance.getSteering(collisionSensor.targets)* collisionAvoidanceWeight;
                accel += cohesion.getSteering(flockingSensor.targets)* cohesionWeight;
                accel += separation.getSteering(flockingSensor.targets)* separationWeight;
                accel += velocityMatch.getSteering(flockingSensor.targets)* velocityMatchWeight;

                steering.steer(accel);
                steering.lookWhereYoureGoing();
            }
            else
            {

                //if (Vector3.Distance(transform.position, playerTransform.position) <= copyPlayerRadius)
                //{
                //    base.MovementControl();
                //}
                //else
                //{
                //    Vector3 accel = steering.arrive(playerTransform.position);
                //    accel += cohesion.getSteering(flockingSensor.targets) * cohesionWeight;
                //    accel += separation.getSteering(flockingSensor.targets) * separationWeight;
                //    accel += velocityMatch.getSteering(flockingSensor.targets) * velocityMatchWeight;
                //    steering.steer(accel);
                //    steering.lookWhereYoureGoing();

                //}

                //Vector3 accel = steering.arrive(playerTransform.position);
                //accel += cohesion.getSteering(flockingSensor.targets) * cohesionWeight;
                //accel += separation.getSteering(flockingSensor.targets) * separationWeight;
                //accel += velocityMatch.getSteering(flockingSensor.targets) * velocityMatchWeight;
                //steering.steer(accel);

                //Debug.Log(playerTransform.rotation);
                //transform.rotation = playerTransform.rotation;

                base.MovementControl();
            }

        }

        protected override void AttackControl()
        {
            base.AttackControl();
        }

        public void Infect()
        {
            if (BIsTriggered)
                return;

            BIsTriggered = true;
            GetComponent<Movement>().BUseController = true;

            mesh.material.SetColor("_RimColor", infectRimColor);
            mesh.material.SetColor("_Color", infectColor);
            mesh.material.SetColor("_HColor", infectHighlightColor);
            mesh.material.SetColor("_SColor", infectShadowColor);

            Instantiate(effectToSpawn, transform.position + Vector3.up * 0.1f, Quaternion.Euler(-90, 0, 0));

            if (playCamShakeOnHit)
            {
                CameraShake.ShakeAll(camShakeOnHit.shakeType,
                    camShakeOnHit.numberOfShakes,
                    camShakeOnHit.shakeAmount,
                    camShakeOnHit.rotationAmount,
                    camShakeOnHit.distance,
                    camShakeOnHit.speed,
                    camShakeOnHit.decay,
                    camShakeOnHit.uiShakeModifier,
                    camShakeOnHit.multiplyByTimeScale);
            }

            MasterAudio.PlaySound(infectSoundName);

        }

    }

}