﻿using System.Collections;
using System.Collections.Generic;
using PixelFr0ggie;
using UnityEngine;

public class GameTester : MonoBehaviour
{
	public void TurnAllEnemies()
	{
		var npc = FindObjectsOfType<AIController>();
		foreach (AIController col in npc)
		{
			col.Infect();
		}
	}
}