﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PixelFr0ggie
{
    public static class SceneManager
    {
        private static SMSceneManager smSceneManager;

        public static SMSceneManager SMSceneManager
        {
            get
            {
                if (smSceneManager == null)
                {
                    smSceneManager = new SMSceneManager(SMSceneConfigurationLoader.LoadActiveConfiguration("Scene Config"));
                    smSceneManager.LevelProgress = new SMLevelProgress(smSceneManager.ConfigurationName);
                    smSceneManager.TransitionPrefab = "Transitions/SMTetrisTransition";
                }

                return smSceneManager;
            }
        }
    }
}