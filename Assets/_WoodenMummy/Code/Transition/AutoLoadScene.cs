﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PixelFr0ggie
{
    public class AutoLoadScene : MonoBehaviour
    {
        public enum Type
        {
            LEVEL,
            SCREEN
        }

        public Type type = Type.LEVEL;
        public string sceneID = "";
        public string transitionPrefabLoc = "";
        public float delay = 2f;

        private float timer = 0f;
        private bool sceneLoaded = false;

        private void Start()
        {
            this.timer = 0f;
            this.sceneLoaded = false;
        }

        private void Update()
        {
            if (this.timer >= this.delay && !this.sceneLoaded)
            {
                if (this.type == Type.LEVEL)
                {
                    if (this.transitionPrefabLoc != "")
                        SceneManager.SMSceneManager.LoadLevel(this.sceneID, this.transitionPrefabLoc);
                    else
                        SceneManager.SMSceneManager.LoadLevel(this.sceneID);
                }
                else
                {
                    if (this.transitionPrefabLoc != "")
                        SceneManager.SMSceneManager.LoadScreen(this.sceneID, this.transitionPrefabLoc);
                    else
                        SceneManager.SMSceneManager.LoadScreen(this.sceneID);
                }

                this.sceneLoaded = true;
            }

            this.timer += Time.deltaTime;
        }

    }
}