﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PixelFr0ggie
{
    public class LoadScene : MonoBehaviour
    {
        public enum Type
        {
            LEVEL,
            SCREEN
        }

        public Type type = Type.LEVEL;
        public string sceneID = "";
        public string transitionPrefabLoc = "";

        public void Activate()
        {
            if (this.type == Type.LEVEL)
            {
                if (this.transitionPrefabLoc != "")
                    SceneManager.SMSceneManager.LoadLevel(this.sceneID, this.transitionPrefabLoc);
                else
                    SceneManager.SMSceneManager.LoadLevel(this.sceneID);
            }
            else
            {
                if (this.transitionPrefabLoc != "")
                    SceneManager.SMSceneManager.LoadScreen(this.sceneID, this.transitionPrefabLoc);
                else
                    SceneManager.SMSceneManager.LoadScreen(this.sceneID);
            }
        }

    }
}