﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
using XboxCtrlrInput;
using XInputDotNetPure;
#endif

public class PlayerManager : MonoBehaviour
{

	public static PlayerManager Instance
	{
		get { return instance; }
	}

	public float LeftMotor
	{
		get
		{
			return leftMotor;
		}

		set
		{
			leftMotor = value;
		}
	}

	public float RightMotor
	{
		get
		{
			return rightMotor;
		}

		set
		{
			rightMotor = value;
		}
	}

	private static PlayerManager instance;

	public GameObject firstPlayer;

	void Awake()
	{
		if (Instance != null)
			Destroy(this);
		else
			instance = this;
	}

	public void VibrateController(float rightModifier, float leftModifier, float time)
	{
		StartCoroutine(Vibrate(rightModifier, leftModifier, time));
	}

	float leftMotor = 0.0f;
	float rightMotor = 0.0f;
	private IEnumerator Vibrate(float rightModifier, float leftModifier, float time)
	{
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		GamePad.SetVibration(0, Mathf.Clamp(leftMotor += leftModifier, 0, 1), Mathf.Clamp(rightMotor += rightModifier, 0, 1));

		yield return new WaitForSeconds(time);

		GamePad.SetVibration(0, Mathf.Clamp(leftMotor -= leftModifier, 0, 1), Mathf.Clamp(rightMotor -= rightModifier, 0, 1));
#else
		yield return new WaitForSeconds(time);

#endif
	}

	void OnDestroy()
	{
		StopAllCoroutines();
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_WIN
		GamePad.SetVibration(0, 0, 0);
#endif
	}
}