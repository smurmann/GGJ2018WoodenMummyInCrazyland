//
// Copyright (c) 2013 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//

using UnityEngine;
using System.Collections;

public class SMInSceneTransitionGUI : MonoBehaviour {

	public GameObject transitionPrefab;

	void OnGUI() {
		GUILayout.BeginArea(new Rect(0,0, Screen.width, Screen.height));
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();

		if (GUILayout.Button("Start In-Scene Transition")) {
			// Instanciate the prefab when the button is pressed. The prefab will start the transition automatically.
			GameObject.Instantiate(transitionPrefab);
			enabled = false; // disable myself
		}

		GUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}
}
