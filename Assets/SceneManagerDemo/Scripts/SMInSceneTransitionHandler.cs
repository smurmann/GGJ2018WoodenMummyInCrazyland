//
// Copyright (c) 2013 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//


using UnityEngine;
using System.Collections;
/// <summary>
/// This class actually handles the transition. In our example, it moves the camera 15 units to the right and then
/// continues the transition.
/// </summary>
public class SMInSceneTransitionHandler : MonoBehaviour {

	public void SMOnTransitionHold() {
		// do whatever scene rearranging logic you want to perform here, while the transition is on hold.
		// You can also delegate this to a different game object. It is also OK to have the logic running
		// for multiple frames.
		Camera.main.transform.position += new Vector3(15f,0f,0f);

		// when you're finished simply call SMTransition.Continue();
		// from wherever you want. It will continue any running transition.
		SMTransition.Continue();
	}
}
