Welcome to Scene Manager!
=============================

Thank you for buying Scene Manager. You can find the quick start tutorial and latest documentation
of Scene Manager on our website http://www.ancientlightstudios.com/scenemanager . 

If you require technical support you can shoot us a mail at support@ancientlightstudios.com .

Demo Projects
-------------

Scene Management Demo
^^^^^^^^^^^^^^^^^^^^^^

Overview
.........
The demo consists of 6 scenes: an intro, a main menu and 4 levels. There are two scene configurations below
Resources/DemoConfig, one resembles a demo version of the game with only 2 levels, the other one the full version
of the game with all 4 levels. 

Running the project
......................

Select the FullGame scene configuration and make it active (unless it is already active). Then run Assets -> Verify
Scene Configuration to make sure the build settings are matching the selected scene configuration. Then open then Intro scene
and press play. The full version of the game is now running. The intro screen is shown and contains a button to go 
to the main menu. In the main menu you have the options to start a new game or reset the progress. Start a new game and
click through to level 3. Then return to the main menu. As you can see, the progress is tracked and the main menu
now offers a third menu entry which allows you to continue playing at level 3. If you want to reset your progress, press
the reset progress button.


Things to try out
.....................

Try to change the transition effect in the Main menu. Have a look at the SMDemoMainMenu class to see how the transition effect
is set at runtime. Try to activate the DemoGame configuration. Then start the Intro level again. You should now be able 
to walk through 2 levels of the game, only. Also notice that the level progress of the demo version is stored at a different 
location as the progress of the full version. Check SMDemoMainMenu class for examples on how to determine if you play the demo 
or the full version and show different menus for each. Finally try to modify the scene configurations or create your own.

In-Scene transition Demo
^^^^^^^^^^^^^^^^^^^^^^^^

This demo consists of a single scene: InSceneTransitionDemo. This scene shows the in-scene transitions feature. You can use this 
feature for cutscenes or for moving the player inside a scene, for switching to a full-screen map of your scene or whatever 
else strikes your fancy. To run the demo, simply open the scene and press the play button. Then click on the 
"Start In-Scene-Transition"  button. 

The scene is controlled by the SMInsceneTransitionGUI script, which starts the transition. The transition
is instanciated from the StayInSceneTransition prefab inside the Resources folder of the demo project. Attached to this prefab
you will find the SMInSceneTransitionHandler which actually reacts to transition events and
moves the camera around while the the transition is in hold state.  That way, the 
cube "magically" turns into a sphere.

