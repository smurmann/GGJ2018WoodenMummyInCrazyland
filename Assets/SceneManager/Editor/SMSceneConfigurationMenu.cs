﻿//
// Copyright (c) 2013 Ancient Light Studios
// All Rights Reserved
//
// http://www.ancientlightstudios.com
//

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UObject = UnityEngine.Object;

/// <summary>
/// Contains menu items for the unity main menu
/// </summary>
public abstract class SMSceneConfigurationMenu {

    [MenuItem("Assets/Verify Scene Configurations")]
    public static void VerifyConfigurations() {
        bool successful = true;
        List<SMSceneConfigurationBase> configurations = SMSceneConfigurationUtil.FindConfigurations();
        Dictionary<string, string> lookup = SMSceneConfigurationUtil.CreateSceneLookup();

        HashSet<string> validScenes = new HashSet<string>();
        foreach(string scene in lookup.Keys) {
            validScenes.Add(scene);
        }

        SMSceneConfigurationBase activeConfiguration = null;
        int activeConfigurations = 0;
        foreach(SMSceneConfigurationBase configuration in configurations) {
            successful &= configuration.IsValid(validScenes);

            if (configuration.activeConfiguration) {
                activeConfigurations++;
                activeConfiguration = configuration;
            }
        }

        if (activeConfigurations == 0) {
            Debug.LogWarning("Currently no scene configuration is active. This will lead to issues when your game is " +
                             "started as Scene Manager doesn't know which scene configuration it should load. Please activate one " +
                             "of your scene configurations. To activate a scene configuration, select it in the project view " +
                             "and then click on the 'Activate' button in the inspector.");
            successful = false;
        }  else if (activeConfigurations > 1) {
            Debug.LogWarning("Currently more than one scene configuration is active. This will lead to issues when your game is " +
                             "started as Scene Manager doesn't know which scene configuration it should load. Please select the configuration you " +
                             "want to keep active in the project view and then press the 'Fix Configuration' button in the inspector. This will deactivate all other " +
                             "scene configurations. To activate another scene configuration, select it in the project view " +
                             "and then click on the 'Activate' button in the inspector.");
            successful = false;
        }  else {
            SMSceneConfigurationUtil.SyncWithBuildSettings(activeConfiguration, lookup);
        }

        if (successful) {
            Debug.Log("All your scene configurations are valid.");
        }
    }

}

