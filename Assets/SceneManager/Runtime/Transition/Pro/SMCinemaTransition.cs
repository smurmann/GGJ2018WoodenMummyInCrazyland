//
// Copyright (c) 2013 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//

using System;
using UnityEngine;
using System.Collections;

[AddComponentMenu("Scripts/SceneManager/Cinema Transition")]
public class SMCinemaTransition : SMPostRenderTransitionWithMobileShader {

	/// <summary>
	/// The size of the border in pixels (if geater than 1) or relative to the screen (otherwise).
	/// </summary>
	public float borderSize = .15f;
	
	/// <summary>
	/// start of the border animation
	/// </summary>
	public float borderStartOffset = 0;
	
	/// <summary>
	/// duration of the border animation
	/// </summary>
	public float borderSlideDuration = 1f;
	
	/// <summary>
	/// start of the tint effect
	/// </summary>
	public float tintStartOffset = 0.5f;
	
	/// <summary>
	/// duration of the tint effect
	/// </summary>
	public float tintDuration = 1f;
	
	/// <summary>
	/// start of the fade out effect
	/// </summary>
	public float fadeOutStartOffset = 2f;
	
	/// <summary>
	/// duration of the fade out effect
	/// </summary>
	public float fadeOutDuration = 1f;
	
	private float actualBorderSize;
	private float duration;
	private float borderProgress;
	private float tintProgress;
	private float fadeProgress;

	protected override string FullShaderName {
		get {
			return "Scene Manager/Cinema Effect";
		}
	}


	protected override void DoPrepare() {
		duration = fadeOutStartOffset + fadeOutDuration;	
		float borderSizeInPixel = SMTransitionUtils.ToAbsoluteSize(borderSize, Screen.height);
		actualBorderSize = borderSizeInPixel / Screen.height;
	}
		
	protected override bool Process(float elapsedTime) {
		float effectTime = elapsedTime;
		// invert direction 
		if (state == SMTransitionState.In) {
			effectTime = duration - effectTime;
		}
		
		borderProgress = SMTransitionUtils.SmoothProgress(borderStartOffset, borderSlideDuration, effectTime);
		tintProgress = SMTransitionUtils.SmoothProgress(tintStartOffset, tintDuration, effectTime);
		fadeProgress = SMTransitionUtils.SmoothProgress(fadeOutStartOffset, fadeOutDuration, effectTime);
		
		return elapsedTime < duration;		
	}
	
	protected override bool NeedsScreenshotForPhase (SMTransitionState state)
	{
		return state == SMTransitionState.Out || state == SMTransitionState.In;
	}

	protected override void DoRender() {
		GL.PushMatrix();
		GL.LoadOrtho();
		GL.LoadIdentity();

	    var flag = EnsureCorrectSrgbConversion();

		DrawImage();
		DrawBorder();
		
        ResetSrgbConversion(flag);

		GL.PopMatrix(); 
	}
	
	private void DrawImage() {	
		shaderMaterial.SetFloat("_TintOffset", tintProgress);
		shaderMaterial.SetFloat("_FadeOffset", fadeProgress);
		for (var i = 0; i < shaderMaterial.passCount; ++i) {
			shaderMaterial.SetPass(i);
			GL.Begin(GL.QUADS);
			GL.TexCoord3(0, 0, 0);
			GL.Vertex3(0, 0, 0);
			GL.TexCoord3(0, 1, 0);
			GL.Vertex3(0, 1, 0);
			GL.TexCoord3(1, 1, 0);
			GL.Vertex3(1, 1, 0);
			GL.TexCoord3(1, 0, 0);
			GL.Vertex3(1, 0, 0);
			GL.End();
		}
	}	
	
	private void DrawBorder() {
		float height = actualBorderSize * borderProgress;
		if (height > 0) {
			for (var i = 0; i < holdMaterial.passCount; ++i) {
				holdMaterial.SetPass(i);
				GL.Begin(GL.QUADS);
				GL.TexCoord3(0, 0, 0);
				GL.Vertex3(0, 1 - height, 0);
				GL.TexCoord3(0, 1, 0);
				GL.Vertex3(0, 1, 0);
				GL.TexCoord3(1, 1, 0);
				GL.Vertex3(1, 1, 0);
				GL.TexCoord3(1, 0, 0);
				GL.Vertex3(1, 1 - height, 0);
				
				GL.TexCoord3(0, 0, 0);
				GL.Vertex3(0, 0, 0);
				GL.TexCoord3(0, 1, 0);
				GL.Vertex3(0, height, 0);
				GL.TexCoord3(1, 1, 0);
				GL.Vertex3(1, height, 0);
				GL.TexCoord3(1, 0, 0);
				GL.Vertex3(1, 0, 0);				
				GL.End();
			}				
		}
	}
	
}

