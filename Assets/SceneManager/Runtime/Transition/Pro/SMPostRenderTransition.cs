//
// Copyright (c) 2013 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//

using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// base class for transitions running in the post render phase
/// </summary>
public abstract class SMPostRenderTransition : SMTransition {
	
	/// <summary>
	/// material used between the fade out and fade in effect
	/// </summary>
	public Material holdMaterial;
		
	private Camera tempCamera;
	private bool reentrantLock = false;
		
	protected virtual void Awake() {
		if (holdMaterial == null) {
			Debug.LogError("'Hold' material is missing");
		}
		
		tempCamera = gameObject.AddComponent<Camera>();
		tempCamera.cullingMask = 0;
		tempCamera.renderingPath = RenderingPath.Forward;
		tempCamera.depth = Mathf.Floor(float.MaxValue);
		tempCamera.clearFlags = CameraClearFlags.Depth;
	}
	
	void OnPostRender() {
		// just to be sure the coroutine is started only once each frame
		if (reentrantLock) {
			return;
		}
		
		reentrantLock = true;
		StartCoroutine(ProcessFrame());
	}
	
	IEnumerator ProcessFrame() {
		yield return new WaitForEndOfFrame();
#if !UNITY_3_5
		if (state == SMTransitionState.Prefetch) {
			reentrantLock = false; // release locks in this case as well
			yield break; // no effects in prefetch state
		}
#endif

#if UNITY_5_6_OR_NEWER
	    // Unity 5.6 doesn't properly clear the depth buffer
	    // for our virtual camera, so we need to do it manually ourselves.
	    GL.Clear(true, false, Color.black);
#endif
		if (state == SMTransitionState.Hold) {
			OnRenderHold();		
		} else {
			OnRender();		
		}
		reentrantLock = false;
	}
	
	protected virtual void OnRenderHold() {
		GL.PushMatrix();
		GL.LoadOrtho();
		GL.LoadIdentity();

	    var flag = EnsureCorrectSrgbConversion();

		for (var i = 0; i < holdMaterial.passCount; ++i) {
			holdMaterial.SetPass(i);
			GL.Begin(GL.QUADS);
			GL.TexCoord3(0, 0, 0);
			GL.Vertex3(0, 0, 0);
			GL.TexCoord3(0, 1, 0);
			GL.Vertex3(0, 1, 0);
			GL.TexCoord3(1, 1, 0);
			GL.Vertex3(1, 1, 0);
			GL.TexCoord3(1, 0, 0);
			GL.Vertex3(1, 0, 0);
			GL.End();
		}

        ResetSrgbConversion(flag);

		GL.PopMatrix(); 
	}
	
	/// <summary>
	/// invoked at the end of each frame
	/// </summary>
	protected abstract void OnRender();

    /// <summary>
    /// This will check whether we're currently in linear color space and force the sRGB conversion on the GL class.
    /// This is to make sure that the correct amount of gamma correction is applied when using linear color space.
    /// It's also a workaround for a bug with Unity's Canvas class, which doesn't seem to reset this flag properly
    /// after rendering. Subclasses should call this before rendering anything with the GL class.
    /// </summary>
    /// <returns>the value needed for <see cref="ResetSrgbConversion"/></returns>
    protected static bool EnsureCorrectSrgbConversion()
    {
        // setting was introduce in Unity 4.3
#if !(UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2) // VR: 4.3
        var result = GL.sRGBWrite;
        if (QualitySettings.activeColorSpace == ColorSpace.Linear)
        {
            GL.sRGBWrite = true;
        }
        return result;
#else
        return false;
#endif
    }
    /// <summary>
    /// Resets the sRGB write flag to it's old value. This is not strictly needed but we're trying to be nice citizens
    /// and clean up our mess afterwards. Subclasses should call this one witht he value returned by <see cref="EnsureCorrectSrgbConversion"/> 
    /// </summary>
    protected static void ResetSrgbConversion(bool oldValue)
    {
#if !(UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2) // VR: 4.3
        GL.sRGBWrite = oldValue;
#endif
    }

}

