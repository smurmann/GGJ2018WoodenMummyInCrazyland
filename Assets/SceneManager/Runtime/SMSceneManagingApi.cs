﻿//
// Copyright (c) 2015 Ancient Light Studios
// All Rights Reserved
// 
// http://www.ancientlightstudios.com
//

#if !UNITY_5_0 && !UNITY_5_1 && !UNITY_5_2 // VR: 5.3
#define SM_UNITY_5_3_OR_LATER
#endif

using UnityEngine;


#if SM_UNITY_5_3_OR_LATER
using UnityEngine.SceneManagement;
#endif


/// <summary>
/// Helper class to abstract Unity scene management API changes in Unity 5.3 away from the code. This class exists
/// to avoid littering the Scene Manager code with ifdefs.
/// </summary>
// ReSharper disable once InconsistentNaming
public static class SMSceneManagingApi
{
    /// <summary>
    /// Returns the name of the currently active scene.
    /// </summary>
    public static string LoadedLevelName
    {
        get
        {
            return
#if SM_UNITY_5_3_OR_LATER
            SceneManager.GetActiveScene().name
#else
            Application.loadedLevelName
#endif
            ;

        }
    }

    /// <summary>
    /// Loads a scene.
    /// </summary>
    /// <param name="name">name of the scene to load.</param>
    public static void LoadScene(string name)
    {
#if SM_UNITY_5_3_OR_LATER
        SceneManager.LoadScene(name, LoadSceneMode.Single);
#else
        Application.LoadLevel(name);
#endif
    }

    /// <summary>
    /// Loads a scene asynchronously.
    /// </summary>
    /// <param name="name">name of the scene to load.</param>
    /// <returns></returns>
    public static AsyncOperation LoadSceneAsync(string name)
    {
#if SM_UNITY_5_3_OR_LATER
        return SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);
#else
        return Application.LoadLevelAsync(name);
#endif

    }
}
