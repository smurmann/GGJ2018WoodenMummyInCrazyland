﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PixelFr0ggie;

public class NearSensor : MonoBehaviour
{
    public LayerMask triggerMask = -1;
    public HashSet<Rigidbody> targets = new HashSet<Rigidbody>();

    void OnTriggerEnter(Collider other)
    {
        if (triggerMask == (triggerMask | (1 << other.gameObject.layer)))
        {
            Rigidbody rb = other.GetComponent<Rigidbody>();
            if (rb)
            {
                targets.Add(rb);

            }
                
        }
        
    }

    void OnTriggerExit(Collider other)
    {
        if (triggerMask == (triggerMask | (1 << other.gameObject.layer)))
        {
            Rigidbody rb = other.GetComponent<Rigidbody>();
            if (rb)
            {
                targets.Remove(rb);
            }
                
        }
            
    }
}
