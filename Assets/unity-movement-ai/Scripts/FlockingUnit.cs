﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Wander2))]
[RequireComponent(typeof(Cohesion))]
[RequireComponent(typeof(Separation))]
[RequireComponent(typeof(VelocityMatch))]
public class FlockingUnit : MonoBehaviour
{
    public float cohesionWeight = 1.5f;
    public float separationWeight = 2f;
    public float velocityMatchWeight = 1f;

    private SteeringBasics steeringBasics;
    private Wander2 wander;
    private Cohesion cohesion;
    private Separation separation;
    private VelocityMatch velocityMatch;

    public NearSensor flockingSensor;

    // Use this for initialization
    void Start()
    {
        steeringBasics = GetComponent<SteeringBasics>();
        wander = GetComponent<Wander2>();
        cohesion = GetComponent<Cohesion>();
        separation = GetComponent<Separation>();
        velocityMatch = GetComponent<VelocityMatch>();

        if (!flockingSensor)
            flockingSensor = transform.Find("Sensor").GetComponent<NearSensor>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 accel = Vector3.zero;

        accel += cohesion.getSteering(flockingSensor.targets) * cohesionWeight;
        accel += separation.getSteering(flockingSensor.targets) * separationWeight;
        accel += velocityMatch.getSteering(flockingSensor.targets) * velocityMatchWeight;

        if (accel.magnitude < 0.005f)
        {
            accel = wander.getSteering();
        }

        steeringBasics.steer(accel);
        steeringBasics.lookWhereYoureGoing();
    }
}
